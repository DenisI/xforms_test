﻿using System;

using Xamarin.Forms;

namespace xForms_Test
{
    public class RssMainPage : ContentPage
    {
        public RssMainPage()
        {
            Content = new StackLayout
            { 
                Children =
                {
                    new Label { Text = "Hello ContentPage" }
                }
            };
        }
    }
}


