﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;
using System.Net;
using System.Linq;
using System.Xml;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;

namespace xForms_Test
{
    public partial class RssMainPage : ContentPage
    {
        
        public static string RssUrl = "http://news.tut.by/rss";

        public RssMainPage()
        {
            InitializeComponent();

            var rssNews = GetNewsFromURL(RssUrl);

            var groupedCustomerList = rssNews.GroupBy(u => u.GroupHeader).Select(gr => new GroupedNewsModel{ longName = gr.Key }.AddRange(gr));

            RssListView.ItemsSource = groupedCustomerList;


        }

        List<NewsDetail> GetNewsFromURL(string url)
        {
            using (var webClient = new WebClient())
            {
                string response = webClient.DownloadString(new Uri(url));
        
                XmlDocument document = new XmlDocument();

                document.LoadXml(response);

                List<NewsDetail> newsFeed = new List<NewsDetail>();
                var rssNodes = document.SelectNodes("rss/channel/item");

                foreach (XmlNode node in rssNodes)
                {
                    var item = new NewsDetail
                    {
                        Title = node.SelectSingleNode("title").InnerText,
                        Description = GetDescription(node.SelectSingleNode("description").InnerText),
                        Link = new Uri(node.SelectSingleNode("link").InnerText),
                        ImageUrl = GetImageURL(node.SelectSingleNode("description").InnerText),
                        PubDate = DateTime.Parse(node.SelectSingleNode("pubDate").InnerText),
                        GroupHeader = GetGroupHeader(node.SelectSingleNode("pubDate").InnerText)
                    };

                    newsFeed.Add(item);
                }

                newsFeed = newsFeed.OrderByDescending(rss => rss.PubDate).ToList<NewsDetail>();

                return newsFeed;
            }
        }

        string GetGroupHeader(string element)
        {
            string header = string.Empty;

            DateTime date = DateTime.Parse(element);

            header = string.Format("{0} in the {1}th hour", date.ToString("dd MMMM"), date.ToString("HH"));

            return header;
        }

        string GetDescription(string element)
        {
            return Regex.Match(element, @">(.*?)<").Groups[1].Value;

        }

        string GetImageURL(string element)
        {
            return Regex.Match(element, @"(?<=img\s+src\=[\x27\x22])(?<Url>[^\x27\x22]*)(?=[\x27\x22])").Groups[1].Value;
        }
    }

    public static class Extensions
    {
        public static ObservableCollection<T> AddRange<T>(this ObservableCollection<T> collection, IEnumerable<T> items)
        {
            items.ToList().ForEach(collection.Add);
            return collection;
        }
    }

    public class ImageFromUrlConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            return ImageSource.FromUri(new Uri(value.ToString()));
        }

        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }

    public class RssListViewClass : ListView
    {
    }

    public class RssScrollViewClass : ScrollView
    {
    }
}

