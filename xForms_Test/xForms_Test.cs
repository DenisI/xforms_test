﻿using System;

using Xamarin.Forms;

namespace xForms_Test
{
    public class App : Application
    {
        public App()
        {
            // The root page of your application
            MainPage = new RssMainPage();
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }

    public class StackLayoutExample: ContentPage
    {
        public StackLayoutExample()
        {
            Padding = new Thickness(20);
            var red = new Label
            {
                Text = "Stop",
                BackgroundColor = Color.Red,
                FontSize = 20
            };
            var yellow = new Label
            {
                Text = "Slow down",
                BackgroundColor = Color.Yellow,
                FontSize = 20
            };
            var green = new Label
            {
                Text = "Go",
                BackgroundColor = Color.Green,
                FontSize = 20
            };

            Content = new StackLayout
            {
                Spacing = 10,
                Children = { red, yellow, green }
            };
        }
    }
}

