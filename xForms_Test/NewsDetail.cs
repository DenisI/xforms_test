﻿using System;
using System.Collections.ObjectModel;

namespace xForms_Test
{


    public class NewsDetail
    {
        public string Title { get; set; }

        public string Description { get; set; }

        public string ImageUrl{ get; set; }

        public Uri Link { get; set; }

        public DateTime PubDate { get; set; }

        public string GroupHeader { get; set; }
    }

    public class GroupedNewsModel : ObservableCollection<NewsDetail>
    {
        public string longName { get; set; }
        public string shortName { get; set; }
    }


}

