﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using UIKit;
using xForms_Test;
using xForms_Test.iOS;
using CoreAnimation;
using CoreGraphics;


[assembly:ExportRenderer(typeof(RssListViewClass), typeof(RssListViewRenderer))]

namespace xForms_Test.iOS
{
    public class RssListViewRenderer : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<ListView> e)
        {
            base.OnElementChanged(e);

            if (Control == null)
                return;

            var listView = Control as UITableView;
            listView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            var gradientLayer = new CAGradientLayer();
            gradientLayer.Frame = listView.Bounds;
            gradientLayer.Colors = new CGColor[]
            { 
                UIColor.Black.CGColor,
                UIColor.Gray.CGColor
            };
            listView.Layer.InsertSublayer(gradientLayer, 0);
        }
    }
}

