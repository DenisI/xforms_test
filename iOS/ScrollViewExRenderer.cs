﻿using System;
using Xamarin.Forms.Platform.iOS;
using xForms_Test.iOS;
using Xamarin.Forms;
using xForms_Test;
using System.ComponentModel;
using UIKit;
using Foundation;

[assembly:ExportRenderer(typeof(RssScrollViewClass), typeof(ScrollViewExRenderer))]

namespace xForms_Test.iOS
{
    public class ScrollViewExRenderer : ScrollViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null || this.Element == null)
                return;

            if (e.OldElement != null)
                e.OldElement.PropertyChanged -= OnElementPropertyChanged;

            e.NewElement.PropertyChanged += OnElementPropertyChanged;
          
        }

        protected void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            
            var scrollView = sender as UIScrollView;
            scrollView.ShowsVerticalScrollIndicator = false;
            scrollView.ShowsHorizontalScrollIndicator = false;

                        
        }
    }
}

